/**
 * Copyright © 2014, Terramenta. All rights reserved.
 *
 * This work is subject to the terms of either
 * the GNU General Public License Version 3 ("GPL") or
 * the Common Development and Distribution License("CDDL") (collectively, the "License").
 * You may not use this work except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://opensource.org/licenses/CDDL-1.0
 * http://opensource.org/licenses/GPL-3.0
 */
package com.terramenta.layermanager.creators;

import java.util.Collection;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import org.openide.util.Lookup;

/**
 *
 * @author chris.heidt
 */
public class LayerCreatorManager {

    private LayerCreatorManager() {

    }

    public static Collection<? extends LayerCreator> getLayerFactories() {
        return Lookup.getDefault().lookupAll(LayerCreator.class);
    }

    public static void display() {
        if (Platform.isFxApplicationThread()) {
            Stage stage = new Stage(StageStyle.UNIFIED);
            stage.setScene(new Scene(createSkin()));
            stage.setAlwaysOnTop(true);
            stage.show();
        } else {
            Platform.runLater(LayerCreatorManager::display);
        }
    }

    private static Parent createSkin() {
        ListView<LayerCreator> left = new ListView<>(FXCollections.observableArrayList(getLayerFactories()));
        left.setCellFactory(new Callback<ListView<LayerCreator>, ListCell<LayerCreator>>() {
            @Override
            public ListCell<LayerCreator> call(ListView<LayerCreator> lv) {
                return new ListCell<LayerCreator>() {
                    @Override
                    public void updateItem(LayerCreator item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item == null || empty ? null : item.getName());
                    }
                };
            }
        });

        StackPane defaultCenterContent = new StackPane(new Label("Select a Layer type."));
        BorderPane root = new BorderPane(defaultCenterContent, null, null, null, left);
        root.setPrefSize(500, 500);

        //change center content on list selection
        left.getSelectionModel().selectedItemProperty().addListener((obs, ov, nv) -> {
            root.setCenter(nv == null ? defaultCenterContent : nv.getSkin());
        });

        return root;
    }

}
