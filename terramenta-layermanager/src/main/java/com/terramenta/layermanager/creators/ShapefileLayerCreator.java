/**
 * Copyright © 2014, Terramenta. All rights reserved.
 *
 * This work is subject to the terms of either
 * the GNU General Public License Version 3 ("GPL") or
 * the Common Development and Distribution License("CDDL") (collectively, the "License").
 * You may not use this work except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://opensource.org/licenses/CDDL-1.0
 * http://opensource.org/licenses/GPL-3.0
 */
package com.terramenta.layermanager.creators;

import com.terramenta.globe.WorldWindManager;
import com.terramenta.globe.layers.KMLLayer;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwindx.examples.util.ShapefileLoader;
import java.io.File;
import java.net.URL;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author chris.heidt
 */
@ServiceProvider(service = LayerCreator.class)
public class ShapefileLayerCreator implements LayerCreator {

    @Override
    public String getName() {
        return "Shapefile";
    }

    @Override
    public Node getSkin() {
        Button browseButton = new Button("Select a SHP file.");
        browseButton.setOnMouseClicked(me -> {
            Window window = browseButton.getScene().getWindow();

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Select a File");
            fileChooser.getExtensionFilters().setAll(
                    new FileChooser.ExtensionFilter("SHP Files", "*.shp")
            );

            File file = fileChooser.showOpenDialog(window);
            if (file != null) {
                gov.nasa.worldwind.formats.shapefile.ShapefileLayerFactory loader =
                        new gov.nasa.worldwind.formats.shapefile.ShapefileLayerFactory();
                Layer layer = (Layer) loader.createFromShapefileSource(file);
                Lookup.getDefault().lookup(WorldWindManager.class).getLayers().add(layer);
                window.hide();
            }
        });
        return new StackPane(browseButton);
    }
}
