/**
 * Copyright © 2014, Terramenta. All rights reserved.
 *
 * This work is subject to the terms of either
 * the GNU General Public License Version 3 ("GPL") or
 * the Common Development and Distribution License("CDDL") (collectively, the "License").
 * You may not use this work except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://opensource.org/licenses/CDDL-1.0
 * http://opensource.org/licenses/GPL-3.0
 */
package com.terramenta.layermanager.creators;

import com.terramenta.globe.WorldWindManager;
import gov.nasa.worldwindx.examples.WMSLayersPanel;
import java.awt.Dimension;
import java.net.URISyntaxException;
import javafx.embed.swing.SwingNode;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author chris.heidt
 */
@ServiceProvider(service = LayerCreator.class)
public class WmsLayerCreator implements LayerCreator {

    private static final WorldWindManager wwm = Lookup.getDefault().lookup(WorldWindManager.class);

    @Override
    public String getName() {
        return "WMS";
    }

    @Override
    public Node getSkin() {
        TextField wmsTextField = new TextField("https://neowms.sci.gsfc.nasa.gov/wms/wms");
        wmsTextField.setPrefWidth(300);

        SwingNode swingNode = new SwingNode();

        Button browseButton = new Button("Refresh");
        browseButton.setOnMouseClicked(me -> {
            try {
                //This is cheating, but i really dont want to recreate this panel in javafx right now.
                WMSLayersPanel wmsLayersPanel = new WMSLayersPanel(wwm.getWorldWindow(), wmsTextField.getText(), new Dimension(600, 600));
                swingNode.setContent(wmsLayersPanel);
            } catch (URISyntaxException ex) {
                //...
            }
        });

        return new StackPane(new VBox(new HBox(wmsTextField, browseButton), swingNode));
    }
}
