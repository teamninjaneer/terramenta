/*
 * Copyright © 2014, Terramenta. All rights reserved.
 *
 * This work is subject to the terms of either
 * the GNU General Public License Version 3 ("GPL") or 
 * the Common Development and Distribution License("CDDL") (collectively, the "License").
 * You may not use this work except in compliance with the License.
 * 
 * You can obtain a copy of the License at
 * http://opensource.org/licenses/CDDL-1.0
 * http://opensource.org/licenses/GPL-3.0
 */
package com.terramenta.globe;

import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.LayerList;
import org.openide.util.Lookup;

/**
 *
 * @author heidtmare
 */
public interface WorldWindManager extends Lookup.Provider {

    /**
     *
     * @return
     */
    WorldWindow getWorldWindow();

    /**
     * Convenience function
     *
     * @return
     */
    LayerList getLayers();

    /**
     *
     * @param that
     */
    void gotoPosition(Position that);

    /**
     *
     * @param that
     * @param animate
     */
    void gotoPosition(Position that, boolean animate);

    /**
     *
     */
    void saveSessionState();

    /**
     *
     */
    void restoreSessionState();

    /**
     *
     * @param newLookup
     * @return
     */
    Lookup addLookup(Lookup newLookup);
}
