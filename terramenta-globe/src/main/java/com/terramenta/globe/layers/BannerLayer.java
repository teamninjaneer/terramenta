/*
 * Copyright © 2014, Terramenta. All rights reserved.
 *
 * This work is subject to the terms of either
 * the GNU General Public License Version 3 ("GPL") or 
 * the Common Development and Distribution License("CDDL") (collectively, the "License").
 * You may not use this work except in compliance with the License.
 * 
 * You can obtain a copy of the License at
 * http://opensource.org/licenses/CDDL-1.0
 * http://opensource.org/licenses/GPL-3.0
 */
package com.terramenta.globe.layers;

import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.layers.AbstractLayer;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;
import gov.nasa.worldwind.render.TextRenderer;
import gov.nasa.worldwind.util.OGLTextRenderer;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import javax.media.opengl.*;

public class BannerLayer extends AbstractLayer implements OrderedRenderable {

    private final Font textFont = new Font("Arial", Font.BOLD, 12);
    private String text = "";
    private Color textColor = Color.CYAN;
    private Color backgroundColor = Color.BLACK;
    private final float[] compArray = new float[4];

    public BannerLayer() {
        super.setName("Banner");
        super.setPickEnabled(false);
        super.setNetworkRetrievalEnabled(false);
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTextColor(String textColorCode) {
        this.textColor = Color.decode(textColorCode);
    }

    public void setBackgroundColor(String textColorCode) {
        this.backgroundColor = Color.decode(textColorCode);
    }

    // Rendering
    @Override
    public void doRender(DrawContext dc) {
        dc.addOrderedRenderable(this);
    }

    @Override
    public void doPick(DrawContext dc, Point pickPoint) {
        // Delegate drawing to the ordered renderable list
        dc.addOrderedRenderable(this);
    }

    @Override
    public double getDistanceFromEye() {
        return 0;
    }

    @Override
    public void pick(DrawContext dc, Point pickPoint) {
        this.draw(dc);
    }

    @Override
    public void render(DrawContext dc) {
        this.draw(dc);
    }

    // Rendering
    public void draw(DrawContext dc) {
        if (text == null || text.isEmpty()) {
            return;
        }

        GL2 gl = dc.getGL().getGL2();
        boolean attribsPushed = false;
        boolean modelviewPushed = false;
        boolean projectionPushed = false;
        try {
            gl.glPushAttrib(GL2.GL_DEPTH_BUFFER_BIT
                    | GL2.GL_COLOR_BUFFER_BIT
                    | GL2.GL_ENABLE_BIT
                    | GL2.GL_TRANSFORM_BIT
                    | GL2.GL_VIEWPORT_BIT
                    | GL2.GL_CURRENT_BIT);
            attribsPushed = true;

            gl.glEnable(GL.GL_BLEND);
            gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
            gl.glDisable(GL.GL_DEPTH_TEST);

            // Load a parallel projection with xy dimensions (viewportWidth, viewportHeight)
            // into the GL projection matrix.
            java.awt.Rectangle viewport = dc.getView().getViewport();
            gl.glMatrixMode(GL2.GL_PROJECTION);
            gl.glPushMatrix();
            projectionPushed = true;
            gl.glLoadIdentity();

            Dimension size = getTextRenderSize(dc, text);
            size.setSize(size.getWidth(), size.getHeight() - 2);//because it is always a bit too big

            if (size.width < viewport.getWidth()) {
                double maxwh = size.width > size.height ? size.width : size.height;
                gl.glOrtho(0d, viewport.width, 0d, viewport.height, -0.6 * maxwh, 0.6 * maxwh);

                gl.glMatrixMode(GL2.GL_MODELVIEW);
                gl.glPushMatrix();
                modelviewPushed = true;
                gl.glLoadIdentity();

                int padding = 3;
                Dimension dimension = new Dimension((int) viewport.getWidth(), (int) size.getHeight() + (padding * 2));
                drawFilledRectangle(
                        dc,
                        new Vec4(0, viewport.getHeight() - dimension.getHeight()),
                        dimension,
                        backgroundColor
                );

                drawLabel(
                        dc,
                        text,
                        new Vec4(viewport.getCenterX() - (size.getWidth() / 2), viewport.getHeight() - size.getHeight() - padding),
                        textColor,
                        textFont
                );
            }
        } finally {
            if (projectionPushed) {
                gl.glMatrixMode(GL2.GL_PROJECTION);
                gl.glPopMatrix();
            }
            if (modelviewPushed) {
                gl.glMatrixMode(GL2.GL_MODELVIEW);
                gl.glPopMatrix();
            }
            if (attribsPushed) {
                gl.glPopAttrib();
            }
        }
    }

    private Dimension getTextRenderSize(DrawContext dc, String text) {
        TextRenderer textRenderer = OGLTextRenderer.getOrCreateTextRenderer(dc.getTextRendererCache(), textFont);
        Rectangle2D nameBound = textRenderer.getBounds(text);
        return nameBound.getBounds().getSize();
    }

    // Draw the label
    private void drawLabel(DrawContext dc, String text, Vec4 screenPoint, Color textColor, Font textFont) {
        int x = (int) screenPoint.x();
        int y = (int) screenPoint.y();
        TextRenderer textRenderer = OGLTextRenderer.getOrCreateTextRenderer(dc.getTextRendererCache(), textFont);
        textRenderer.begin3DRendering();
        textRenderer.setColor(this.getBackgroundColor(textColor));
        textRenderer.draw(text, x + 1, y - 1);//back shadow
        textRenderer.setColor(textColor);
        textRenderer.draw(text, x, y);//label
        textRenderer.end3DRendering();
    }

    // Compute background color for best contrast
    private Color getBackgroundColor(Color color) {
        Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), compArray);
        if (compArray[2] > 0.5) {
            return new Color(0, 0, 0, 0.7f);
        } else {
            return new Color(1, 1, 1, 0.7f);
        }
    }

    private void drawFilledRectangle(DrawContext dc, Vec4 origin, Dimension dimension, Color color) {
        GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.
        gl.glColor4ub((byte) color.getRed(), (byte) color.getGreen(),
                (byte) color.getBlue(), (byte) color.getAlpha());
        gl.glBegin(GL2.GL_POLYGON);
        gl.glVertex3d(origin.x, origin.y, 0);
        gl.glVertex3d(origin.x + dimension.getWidth(), origin.y, 0);
        gl.glVertex3d(origin.x + dimension.getWidth(), origin.y + dimension.getHeight(), 0);
        gl.glVertex3d(origin.x, origin.y + dimension.getHeight(), 0);
        gl.glVertex3d(origin.x, origin.y, 0);
        gl.glEnd();
    }
}
